The Cosmetic Concierge is a state-of-the-art Cosmetic Surgery Center that offers a relaxed, spa-like atmosphere. Dr. Hope Sherie, our founder, is a board-certified surgeon with nearly two decades of experience and an impressive natural gift for creating dazzling results.

Address: 325 Arlington Avenue, Charlotte, NC 28203, USA

Phone: 980-938-0459